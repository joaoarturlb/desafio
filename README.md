# Desafio

#### Api desenvolvida para o teste do processo seletido do Inter.

### Contato

- [João Artur](https://gitlab.com/joaoarturlb)

- [joaoarturlb@gmail.com](mailto:joaoarturlb@gmail.com)

- [(31) 99908-7343](callto:+5531999087343)

 

## Requisitos

Para build e execução da aplicação é necessário:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Execução dos Testes Unitários

Usando o [Maven 3.6](https://maven.apache.org) para rodar os testes.

```bash
mvn test 
```

## Execução local

Use o [Maven 3.6](https://maven.apache.org) para iniciar a aplicação.
```bash
mvn spring-boot:run
```

## Execução do build/compile e gerando o .war

Use o [Maven 3.6](https://maven.apache.org) para buildar/compilar a aplicação.

Onde executará os testes também.

```bash
mvn clean install
```

## Uso
### Para execução local

Url base da API ```http://localhost:8080```

### Para deploy do .war no servidor

Url base da API ```http://{ip_or_host_server}:{port_server}/{context_name}```

## API Docs

Arquivo de docs com a especificação da API com o nome de ***api-docs.yaml***

## Testes Integrado com o Postmam

Arquivo para teste integrado com o postman na raiz do projeto com nome de ***postman_collection.json***