package br.com.bancointer.desafio;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import org.junit.jupiter.api.Test;

import br.com.bancointer.desafio.model.DigitoUnico;
import br.com.bancointer.desafio.security.Criptografia;
import br.com.bancointer.desafio.util.DigitoUnicoFactory;

class DesafioApplicationTests {

	private final static Logger logger = Logger.getLogger(DesafioApplicationTests.class.getName());

	@Test
	public void contextLoads() {

	}

	@Test
	public void testCalculaDigitoUnico() {

		logger.info("Iniciando teste de calculo de digito unico.");

		List<DigitoUnico> resultado = new ArrayList<DigitoUnico>();

		resultado.add(new DigitoUnico("351488266", 78345, 9));
		resultado.add(new DigitoUnico("2093560089", 5726, 3));
		resultado.add(new DigitoUnico("1595879777", 79265, 4));
		resultado.add(new DigitoUnico("682275926", 84507, 3));
		resultado.add(new DigitoUnico("1240583998", 59038, 1));

		for (DigitoUnico digitoUnico : resultado) {
			assertEquals(digitoUnico.getResultado(),
					DigitoUnicoFactory.calculaDigitoUnico(digitoUnico.getN(), digitoUnico.getK()));
		}
	}

	@Test
	public void testCacheDigitoUnico() {

		logger.info("Iniciando teste de CACHE do calculo de digito unico.");

		final int NUMERO_DE_DIGITO_UNICOS = 5;

		Random random = new Random();
		int iMin = 1;
		int iMax = (int) Math.pow(10, 1000000);

		int kMin = 1;
		int kMax = (int) Math.pow(10, 5);

		LinkedList<DigitoUnico> listDigitoUnico = new LinkedList<DigitoUnico>();

		for (int i = 0; i < NUMERO_DE_DIGITO_UNICOS; i++) {

			Integer nInt = random.nextInt(iMax - iMin) + iMin;
			Integer kInt = random.nextInt(kMax - kMin) + kMin;

			Integer resultDu = DigitoUnicoFactory.calculaDigitoUnico(nInt.toString(), kInt);

			listDigitoUnico.add(new DigitoUnico(nInt.toString(), kInt, resultDu));
			
			if (DigitoUnicoFactory.digitoUnicoCache.getLast().getResultado().equals(
					listDigitoUnico.getLast().getResultado())) {
				logger.info("Calculo em cache.");
			}
			
			assertEquals(DigitoUnicoFactory.digitoUnicoCache.getLast().getResultado(),
					listDigitoUnico.getLast().getResultado());

		}
	}
	
	@Test
	public void testEncryptAndDecrypt() {
		
		String painText = "ISSO É UM TESTE DE ENCRIPTAÇÃO E DESCRIPTAÇÃO";
		
		KeyPair keyPair = Criptografia.generateKeyPair();
		
		PublicKey publicKey = keyPair.getPublic();
		PrivateKey privateKey = keyPair.getPrivate();
		
		String encryptedText = Criptografia.encrypt(painText, publicKey);
		
		String stringEncryptAndDecrypt = Criptografia.decrypt(encryptedText, privateKey);
		
		logger.info(stringEncryptAndDecrypt);
		
		assertEquals(painText, stringEncryptAndDecrypt);
		
		
	}
}
