package br.com.bancointer.desafio.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.bancointer.desafio.model.User;
import br.com.bancointer.desafio.service.SettingService;
import br.com.bancointer.desafio.service.UserService;

@Controller
@RequestMapping(value="/user", produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping
	public List<User> getAllUsers() {

		return userService.getAllUsers();
	}

	@GetMapping("/{id}")
	public User getUser(@PathVariable("id") Long id, @RequestBody(required = false) Map<String, String> privateKeyRequest) {

		
		if (privateKeyRequest != null) {
			String privateKeyString = privateKeyRequest.get(SettingService.NAME_PRIVATE_KEY_SYSTEM);
			return userService.getUserByIdWithPrivateKey(id, privateKeyString);
		}
		
		return userService.getUserById(id);
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Long createUser(@RequestBody User user) {

		return userService.createUser(user).getId();
	}

	@PutMapping("/{id}")
	public Long updateUser(@PathVariable("id") Long idFind, @RequestBody User user) {

		return userService.updateUser(idFind, user);
	}
	
	@DeleteMapping("/{id}")
	public Map<String, String> deleteUser(@PathVariable("id") Long id) {
		
		Map<String, String> response = new LinkedHashMap<String, String>();
		
		Boolean deleted = userService.deleteUser(id);
		
		response.put("id", id.toString());
		response.put("deleted", deleted.toString());
		
		return response;
	}

}
