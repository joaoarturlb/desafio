package br.com.bancointer.desafio.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import br.com.bancointer.desafio.model.PublicKeys;
import br.com.bancointer.desafio.model.User;
import br.com.bancointer.desafio.service.PublicKeysService;
import br.com.bancointer.desafio.service.SettingService;
import br.com.bancointer.desafio.service.UserService;

@Controller
@RequestMapping(value = "/chave-publica", produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public class PublicKeysController {

	@Autowired
	PublicKeysService publicKeysService;

	@Autowired
	UserService userService;

	@PostMapping(value = "/{user_id}")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, String> createPublicKeys(@PathVariable("user_id") Long idUser,
			@RequestBody Map<String, String> publicKeyRequest) {

		String publicKeyString = publicKeyRequest.get(SettingService.NAME_PUBLIC_KEY_SYSTEM);
		
		Map<String, String> response = new LinkedHashMap<String, String>();

		if (publicKeysService.verificaChavePublica(publicKeyString)) {

			User user = userService.getUserById(idUser);

			if (user.getId() == 0L) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuário não encontrado na base com id: " + idUser);
			}

			PublicKeys publicKeys = new PublicKeys(publicKeyString, user);
			publicKeysService.createPublicKeys(publicKeys);

			publicKeysService.reencriptaDadoUser(publicKeys);

			response.put("chaveAdd", "true");
			response.put("idUser", idUser.toString());
		} else {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Chave Publica informada inválida."); 
		}

		return response;
	}

}
