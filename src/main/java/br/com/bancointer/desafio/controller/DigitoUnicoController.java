package br.com.bancointer.desafio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.bancointer.desafio.model.DigitoUnico;
import br.com.bancointer.desafio.model.User;
import br.com.bancointer.desafio.service.DigitoUnicoService;
import br.com.bancointer.desafio.service.UserService;
import br.com.bancointer.desafio.util.DigitoUnicoFactory;

@Controller
@RequestMapping(value="/digito-unico", produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public class DigitoUnicoController {

	
	
	@Autowired
	private DigitoUnicoService digitoUnicoService;

	@Autowired
	private UserService userService;
	
	@GetMapping
	public List<DigitoUnico> getAllDigitoUnicoOrByUser(@RequestParam(name = "user_id", required = false) Long idUser) {

		if (idUser == null) {
			return digitoUnicoService.getAll();
		}

		return digitoUnicoService.getDigitoUnicoByUser(idUser);
	}

	@GetMapping("/{id}")
	public DigitoUnico getById(@PathVariable("id") Long id) {

		return digitoUnicoService.getById(id);
	}

	@PostMapping("/calcula")
	public Integer calculaDigitoUnico(@RequestParam(name = "n") String nInput, @RequestParam(name = "k") Integer kInput,
			@RequestParam(name = "user_id", required = false) Long idUser) {

		if (idUser == null) {
			
			return DigitoUnicoFactory.calculaDigitoUnico(nInput, kInput);
			
		}
		
		User user = userService.getUserById(idUser);
		
		if (user.getId() == 0L) {
			return DigitoUnicoFactory.calculaDigitoUnico(nInput, kInput);
		}
		
		Integer resultadoDigitoUnico = DigitoUnicoFactory.calculaDigitoUnico(nInput, kInput);
		
		DigitoUnico duAdd = new DigitoUnico(nInput, kInput, resultadoDigitoUnico);
		
		if (digitoUnicoService.isExistOnDataBase(nInput, kInput, idUser)) {
			return resultadoDigitoUnico;
		}
		
		user.getListDigitoUnico().add(digitoUnicoService.createDigitoUnico(duAdd));
		
		userService.updateUser(user.getId(), user);
		
		return resultadoDigitoUnico;
	}

}
