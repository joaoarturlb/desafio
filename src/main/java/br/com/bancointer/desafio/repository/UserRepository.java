package br.com.bancointer.desafio.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.bancointer.desafio.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
