package br.com.bancointer.desafio.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.bancointer.desafio.model.DigitoUnico;

@Repository
public interface DigitoUnicoRepository extends CrudRepository<DigitoUnico, Long> {
	
	
	@Query("SELECT du FROM digitoUnico du WHERE user_id = ?1")
	List<DigitoUnico> getAllByUser(Long idUser);

	@Query("SELECT du FROM digitoUnico du WHERE n = ?1 and k = ?2 and user_id = ?3")
	Optional<DigitoUnico> getIfExists(String n, Integer k, Long idUser);
	
}
