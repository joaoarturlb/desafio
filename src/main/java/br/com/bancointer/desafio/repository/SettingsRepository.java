package br.com.bancointer.desafio.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.bancointer.desafio.model.Settings;

@Repository
public interface SettingsRepository extends CrudRepository<Settings, Long> {

	Optional<Settings> findByKey(String key);
	
}
