package br.com.bancointer.desafio.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.bancointer.desafio.model.PublicKeys;
import br.com.bancointer.desafio.model.User;

@Repository
public interface PublicKeysRepository extends CrudRepository<PublicKeys, Long> {

	Optional<PublicKeys> findByUser(User user);
	
}
