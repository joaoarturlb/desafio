package br.com.bancointer.desafio.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.bancointer.desafio.model.DigitoUnico;
import br.com.bancointer.desafio.repository.DigitoUnicoRepository;

@Service
public class DigitoUnicoService {

	@Autowired
	private DigitoUnicoRepository repository;

	public List<DigitoUnico> getAll() {

		List<DigitoUnico> listDu = (List<DigitoUnico>) repository.findAll();

		if (listDu.isEmpty()) {
			return new ArrayList<DigitoUnico>();
		}

		return listDu;
	}

	public DigitoUnico getById(Long id) {

		return repository.findById(id).orElseThrow(
				() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "DigitoUnico não encotrado com id: " + id));

	}

	public List<DigitoUnico> getDigitoUnicoByUser(Long idUser) {

		return repository.getAllByUser(idUser);
	}

	public DigitoUnico createDigitoUnico(DigitoUnico digitoUnico) {

		if (digitoUnico.getId() != 0L) {
			Optional<DigitoUnico> du = repository.findById(digitoUnico.getId());
			if (du.isPresent()) {
				throw new ResponseStatusException(HttpStatus.CONFLICT,
						"Calculo de digito existente na base com id: " + du.get().getId());
			} else {
				return repository.save(digitoUnico);
			}
		}

		return repository.save(digitoUnico);

	}

	public Boolean isExistOnDataBase(String n, Integer k, Long idUser) {

		Optional<DigitoUnico> du = repository.getIfExists(n, k, idUser);

		if (du.isPresent()) {
			return true;
		}
		return false;

	}

}
