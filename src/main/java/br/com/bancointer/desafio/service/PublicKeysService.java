package br.com.bancointer.desafio.service;

import java.security.PublicKey;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.bancointer.desafio.model.PublicKeys;
import br.com.bancointer.desafio.model.User;
import br.com.bancointer.desafio.repository.PublicKeysRepository;
import br.com.bancointer.desafio.security.Criptografia;

@Service
public class PublicKeysService {

	@Autowired
	PublicKeysRepository repository;

	@Autowired
	UserService userService;

	@Autowired
	SettingService settingService;

	public PublicKeys createPublicKeys(PublicKeys publicKeys) {

		Optional<PublicKeys> publicKeysExist = repository.findByUser(publicKeys.getUser());

		if (publicKeysExist.isPresent()) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Já existe chave para esse usuário.");
		}

		return repository.save(publicKeys);
	}

	public PublicKeys getPublicKeysByUser(User user) {

		Optional<PublicKeys> publicKeysExist = repository.findByUser(user);

		if (publicKeysExist.isPresent()) {
			return publicKeysExist.get();
		}

		return new PublicKeys();

	}

	public void reencriptaDadoUser(PublicKeys publicKeys) {

		User user = publicKeys.getUser();
		userService.updateUser(user.getId(), user);
	}

	public Boolean verificaChavePublica(String publicKeyString) {

		final String ALGORITHM = "RSA";
		final int LENGTH_KEY = 2048;

		PublicKey publicKey = Criptografia.convertStringToPublicKey(publicKeyString);

		if (!publicKey.getAlgorithm().equalsIgnoreCase(ALGORITHM)) {
			return false;
		}

		if (Criptografia.getLengthPublicKey(publicKey) != LENGTH_KEY) {
			return false;
		}

		return true;
	}

}
