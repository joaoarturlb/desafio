package br.com.bancointer.desafio.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.bancointer.desafio.model.Settings;
import br.com.bancointer.desafio.repository.SettingsRepository;

@Service
public class SettingService {

	public static final String NAME_PRIVATE_KEY_SYSTEM = "privateKey";

	public static final String NAME_PUBLIC_KEY_SYSTEM = "publicKey";

	@Autowired
	SettingsRepository repository;

	public List<Settings> getAll() {

		List<Settings> listSettings = (List<Settings>) repository.findAll();

		if (listSettings.isEmpty()) {
			new ArrayList<Settings>();
		}
		return listSettings;
	}

	public Settings getByKey(String key) {

		Optional<Settings> setting = repository.findByKey(key);

		if (setting.isPresent()) {
			return setting.get();
		}

		return new Settings();

	}

	public Settings createSetting(Settings settings) {

		Optional<Settings> settingExist = repository.findByKey(settings.getKey());

		if (settingExist.isPresent()) {
			throw new ResponseStatusException(HttpStatus.CONFLICT,
					"Key já existe na tabela Striings: " + settings.getKey());
		} else {
			return repository.save(settings);
		}

	}

}
