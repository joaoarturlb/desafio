package br.com.bancointer.desafio.service;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.bancointer.desafio.model.DigitoUnico;
import br.com.bancointer.desafio.model.PublicKeys;
import br.com.bancointer.desafio.model.User;
import br.com.bancointer.desafio.repository.UserRepository;
import br.com.bancointer.desafio.security.Criptografia;

@Service
public class UserService {

	private static final Logger logger = Logger.getLogger(UserService.class.getName());

	@Autowired
	UserRepository repository;

	@Autowired
	PublicKeysService publicKeysService;

	@Autowired
	SettingService settingService;

	public List<User> getAllUsers() {

		List<User> users = (List<User>) repository.findAll();

		if (users.isEmpty()) {
			return new ArrayList<User>();
		}

		List<User> responseUsers = new ArrayList<User>();

		PrivateKey key;

		for (User user : users) {

			PublicKeys pubKeys = publicKeysService.getPublicKeysByUser(user);

			if (pubKeys.getId() == 0L) {

				String privateKeySystemString = settingService.getByKey(SettingService.NAME_PRIVATE_KEY_SYSTEM)
						.getObject();
				key = Criptografia.convertStringToPrivateKey(privateKeySystemString);

				user.setName(Criptografia.decrypt(user.getName(), key));
				user.setEmail(Criptografia.decrypt(user.getEmail(), key));
			}

			responseUsers.add(user);

		}

		return responseUsers;
	}

	public User getUserById(Long id) {

		Optional<User> userOptional = repository.findById(id);

		if (userOptional.isPresent()) {

			User user = userOptional.get();

			PublicKeys pubKeys = publicKeysService.getPublicKeysByUser(user);

			if (pubKeys.getId() == 0L) {

				String privateKeySystemString = settingService.getByKey(SettingService.NAME_PRIVATE_KEY_SYSTEM)
						.getObject();
				PrivateKey key = Criptografia.convertStringToPrivateKey(privateKeySystemString);

				user.setName(Criptografia.decrypt(user.getName(), key));
				user.setEmail(Criptografia.decrypt(user.getEmail(), key));
			}
			return user;
		}

		logger.info("Usuário não encontrado na base, com id: " + id);
		return new User();

	}

	public User getUserByIdWithPrivateKey(Long id, String privateKeyUserString) {

		Optional<User> userOptional = repository.findById(id);

		if (userOptional.isPresent()) {

			User user = userOptional.get();

			PublicKeys pubKeys = publicKeysService.getPublicKeysByUser(user);

			if (pubKeys.getId() == 0L) {

				String privateKeySystemString = settingService.getByKey(SettingService.NAME_PRIVATE_KEY_SYSTEM)
						.getObject();
				PrivateKey key = Criptografia.convertStringToPrivateKey(privateKeySystemString);

				user.setName(Criptografia.decrypt(user.getName(), key));
				user.setEmail(Criptografia.decrypt(user.getEmail(), key));

			} else {

				PrivateKey key = Criptografia.convertStringToPrivateKey(privateKeyUserString);

				user.setName(Criptografia.decrypt(user.getName(), key));
				user.setEmail(Criptografia.decrypt(user.getEmail(), key));

			}
			return user;
		}

		logger.info("Usuário não encontrado na base, com id: " + id);
		return new User();

	}

	public User createUser(User user) {

		String publicKeySystemString = settingService.getByKey(SettingService.NAME_PUBLIC_KEY_SYSTEM).getObject();
		PublicKey publicKeySystem = Criptografia.convertStringToPublicKey(publicKeySystemString);

		user.setName(Criptografia.encrypt(user.getName(), publicKeySystem));
		user.setEmail(Criptografia.encrypt(user.getEmail(), publicKeySystem));

		if (user.getId() != 0L) {
			Optional<User> u = repository.findById(user.getId());
			if (u.isPresent()) {
				throw new ResponseStatusException(HttpStatus.CONFLICT,
						"Usuário existente na base com id: " + u.get().getId());
			} else {

				return repository.save(user);
			}
		}

		return repository.save(user);
	}

	public Long updateUser(Long id, User user) {

		Optional<User> userBase = repository.findById(id);

		if (userBase.isPresent()) {

			PublicKey key;
			PublicKeys pubKeys = publicKeysService.getPublicKeysByUser(userBase.get());

			if (pubKeys.getId() == 0L) {
				String publicKeySystemString = settingService.getByKey(SettingService.NAME_PUBLIC_KEY_SYSTEM)
						.getObject();
				key = Criptografia.convertStringToPublicKey(publicKeySystemString);
			} else {

				key = Criptografia.convertStringToPublicKey(pubKeys.getPublicKey());
			}

			User userRaplace = new User();
			userRaplace.setName(Criptografia.encrypt(user.getName(), key));
			userRaplace.setEmail(Criptografia.encrypt(user.getEmail(), key));
			userRaplace.setId(id);

			if (user.getListDigitoUnico() == null) {
				user.setListDigitoUnico(new ArrayList<DigitoUnico>());
			}
			
			if (!user.getListDigitoUnico().isEmpty()) {
				userRaplace.setListDigitoUnico(user.getListDigitoUnico());
			} else {
				userRaplace.setListDigitoUnico(userBase.get().getListDigitoUnico());
			}
			return repository.save(userRaplace).getId();
		} else {

			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuário não encontrado na base com id: " + id);

		}

	}

	public boolean deleteUser(Long id) {

		Optional<User> user = repository.findById(id);

		if (user.isPresent()) {

			repository.delete(user.get());

			return true;
		} else {

			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuário não encontrado na base com id: " + id);
		}

	}

}
