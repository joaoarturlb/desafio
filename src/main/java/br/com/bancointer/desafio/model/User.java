package br.com.bancointer.desafio.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	
	@Column(columnDefinition = "NVARCHAR(MAX)")
	private String name;

	@Column(columnDefinition = "NVARCHAR(MAX)")
	private String email;
	
	@OneToMany
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private List<DigitoUnico> listDigitoUnico;

	public User(String name, String email, List<DigitoUnico> listDigitoUnico) {

		this.setEmail(email);
		this.setName(name);
		this.setListDigitoUnico(listDigitoUnico);

	}
	
	public User(String name, String email) {

		this.setEmail(email);
		this.setName(name);
	}

	public User() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setListDigitoUnico(List<DigitoUnico> listDigitoUnico) {
		this.listDigitoUnico = listDigitoUnico;
	}
	
	public List<DigitoUnico> getListDigitoUnico() {
		return listDigitoUnico;
	}	
	
	@Override
	public String toString() {
		
		return "User: { nome: "+ this.getName() + ", email : " + this.getEmail() + " }";
	}
}