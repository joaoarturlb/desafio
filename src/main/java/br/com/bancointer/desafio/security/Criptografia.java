package br.com.bancointer.desafio.security;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import br.com.bancointer.desafio.service.SettingService;

public class Criptografia {

	private static final String ALGORITHM = "RSA";
	private static final int LENGTH_KEY = 2048;

	public static KeyPair generateKeyPair() {

		KeyPair keyPair;

		try {
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM);
			keyPairGenerator.initialize(LENGTH_KEY);
			keyPair = keyPairGenerator.generateKeyPair();
			return keyPair;
		} catch (NoSuchAlgorithmException e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Erro ao criar ao criar o par de chaves.", e);
		}
	}

	public static Map<String, String> geraKeyParToString(KeyPair keyPair) {

		Map<String, String> result = new LinkedHashMap<String, String>();

		PrivateKey privateKey = keyPair.getPrivate();
		PublicKey publicKey = keyPair.getPublic();

		String privateKeyString = Base64.getEncoder().withoutPadding().encodeToString(privateKey.getEncoded());
		String publicKeyString = Base64.getEncoder().withoutPadding().encodeToString(publicKey.getEncoded());

		result.put(SettingService.NAME_PRIVATE_KEY_SYSTEM, privateKeyString);
		result.put(SettingService.NAME_PUBLIC_KEY_SYSTEM, publicKeyString);

		return result;
	}

	public static PublicKey convertStringToPublicKey(String publicKeyString) {

		PublicKey publicKey;
		try {
			X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyString));
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
			publicKey = keyFactory.generatePublic(publicSpec);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {

			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Não foi possível converter string para PublicKey.", e);
		}

		return publicKey;
	}

	public static PrivateKey convertStringToPrivateKey(String privateKeyString) {

		PrivateKey privateKey;
		try {
			PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyString));
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
			privateKey = keyFactory.generatePrivate(privateSpec);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {

			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Não foi possível converter string para PrivateKey.", e);
		}

		return privateKey;
	}

	public static String encrypt(String plainText, PublicKey publicKey) {

		final String charSet = "UTF-8";

		String encryptedText;

		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			encryptedText = Base64.getEncoder().encodeToString(cipher.doFinal(plainText.getBytes(charSet)));

		} catch (UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException
				| NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {

			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Erro ao encriptar o texto.", e);
		}
		return encryptedText;
	}

	public static String decrypt(String encryptedText, PrivateKey privateKey) {

		final String charSet = "UTF-8";

		String plainText;

		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			plainText = new String(cipher.doFinal(Base64.getDecoder().decode(encryptedText)), charSet);

		} catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | UnsupportedEncodingException e) {

			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Erro ao decriptografar texto.", e);
		}

		return plainText;
	}

	public static Integer getLengthPublicKey(PublicKey key) {

		String publicKeyString = key.toString();

		String[] result = publicKeyString.split(" bits");

		String lengthKey = result[0].split("key, ")[1];

		return Integer.parseInt(lengthKey);

	}

	public static void generateKeyPairOnFile(String filePath) {

		KeyPair keyPair = generateKeyPair();

		Map<String, String> result = geraKeyParToString(keyPair);

		String privateKeyString = result.get(SettingService.NAME_PRIVATE_KEY_SYSTEM);
		String publicKeyString = result.get(SettingService.NAME_PUBLIC_KEY_SYSTEM);

		try {
			BufferedWriter writerPrivateKeyFile = new BufferedWriter(new FileWriter(filePath + "_privateKey.txt"));
			writerPrivateKeyFile.write(privateKeyString);
			writerPrivateKeyFile.close();

			BufferedWriter writerPublicKeyFile = new BufferedWriter(new FileWriter(filePath + "_publicKey.txt"));
			writerPublicKeyFile.write(publicKeyString);
			writerPublicKeyFile.close();

		} catch (IOException e) {

			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Error ao salvar par de chaves em arquivo", e);
		}

	}

}
