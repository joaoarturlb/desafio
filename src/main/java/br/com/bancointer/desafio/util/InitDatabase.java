package br.com.bancointer.desafio.util;

import java.security.KeyPair;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import br.com.bancointer.desafio.model.DigitoUnico;
import br.com.bancointer.desafio.model.Settings;
import br.com.bancointer.desafio.model.User;
import br.com.bancointer.desafio.security.Criptografia;
import br.com.bancointer.desafio.service.DigitoUnicoService;
import br.com.bancointer.desafio.service.SettingService;
import br.com.bancointer.desafio.service.UserService;

public class InitDatabase {

	private static Logger logger = Logger.getLogger(InitDatabase.class.getName());

	public static void initUserAndListDigitoUnico(UserService userService, DigitoUnicoService digitoUnicoService) {

		final int NUMERO_DE_DIGITO_UNICOS = 2;
		Random random = new Random();
		int iMin = 1;
		int iMax = (int) Math.pow(10, 1000000);

		int kMin = 1;
		int kMax = (int) Math.pow(10, 5);

		User user1 = new User("João Artur", "joao.artur@bancointer.com.br");
		User user2 = new User("Maria José", "maria.jose@bancointer.com.br");

		List<DigitoUnico> listDigitoUnico = new LinkedList<DigitoUnico>();

		logger.info("Criando lista de digito único e persistindo no banco.");

		for (int i = 0; i < NUMERO_DE_DIGITO_UNICOS; i++) {

			Integer nInt = random.nextInt(iMax - iMin) + iMin;
			Integer kInt = random.nextInt(kMax - kMin) + kMin;

			Integer resultDu = DigitoUnicoFactory.calculaDigitoUnico(nInt.toString(), kInt);

			DigitoUnico du = new DigitoUnico(nInt.toString(), kInt, resultDu);

			DigitoUnico duSave = digitoUnicoService.createDigitoUnico(du);

			listDigitoUnico.add(duSave);

		}

		user1.setListDigitoUnico(listDigitoUnico);

		listDigitoUnico = new LinkedList<DigitoUnico>();

		for (int i = 0; i < NUMERO_DE_DIGITO_UNICOS; i++) {

			Integer nInt = random.nextInt(iMax - iMin) + iMin;
			Integer kInt = random.nextInt(kMax - kMin) + kMin;

			Integer resultDu = DigitoUnicoFactory.calculaDigitoUnico(nInt.toString(), kInt);

			DigitoUnico du = new DigitoUnico(nInt.toString(), kInt, resultDu);

			DigitoUnico duSave = digitoUnicoService.createDigitoUnico(du);

			listDigitoUnico.add(duSave);

		}

		user2.setListDigitoUnico(listDigitoUnico);

		logger.info("Persistindo os usuários no banco com a lista de digito único criada.");

		userService.createUser(user1);
		userService.createUser(user2);

		logger.info("Usuários no banco:");
		userService.getAllUsers().forEach(user -> logger.info(user.toString()));

	}

	public static void initSettings(SettingService settingService) {

		logger.info("Criando par de chaves.");

		KeyPair keyPair = Criptografia.generateKeyPair();

		Map<String, String> keyPairMapString = Criptografia.geraKeyParToString(keyPair);

		Settings settingPrivateKey = new Settings(SettingService.NAME_PRIVATE_KEY_SYSTEM,
				keyPairMapString.get(SettingService.NAME_PRIVATE_KEY_SYSTEM));
		Settings settingPublicKey = new Settings(SettingService.NAME_PUBLIC_KEY_SYSTEM,
				keyPairMapString.get(SettingService.NAME_PUBLIC_KEY_SYSTEM));

		logger.info("Persistindo par de chaves no banco.");

		if (settingService.getByKey(SettingService.NAME_PRIVATE_KEY_SYSTEM).getId() == null) {
			settingService.createSetting(settingPrivateKey);
		} else {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "PrivateKey do sistema já existe no banco.");
		}

		if (settingService.getByKey(SettingService.NAME_PUBLIC_KEY_SYSTEM).getId() == null) {
			settingService.createSetting(settingPublicKey);
		} else {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "PublicKey do sistema já existe no banco.");
		}

	}

}
