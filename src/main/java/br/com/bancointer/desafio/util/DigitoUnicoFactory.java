package br.com.bancointer.desafio.util;

import java.util.LinkedList;

import br.com.bancointer.desafio.model.DigitoUnico;

public class DigitoUnicoFactory {

	
	public static LinkedList<DigitoUnico> digitoUnicoCache = new LinkedList<DigitoUnico>();
	
	private static Integer calculaDigitoUnicoSemCache(String n, Integer k) {
		
		

		if (validaInput(n, k)) {
			
			int nInt = Integer.parseInt(n);
						
			String P  = "";
		
			for (int i = 0; i < k; i++) {	
				P = P.concat(n);	
			}
			
			if (P.length() == 1) {
				return nInt;
			}
			
			return recDigitoUnico(P);
						
		}

		return 0;
	}

	public static Integer calculaDigitoUnico(String n, Integer k) {
		
		int lengthListDU = digitoUnicoCache.size();
		
		for (DigitoUnico digitoUnico : digitoUnicoCache) {
			if (n.equals(digitoUnico.getN())) {
				if (k.equals(digitoUnico.getK())) {
					return digitoUnico.getResultado();
				}
			}
		}
		
		DigitoUnico resultDU = new DigitoUnico(n, k, DigitoUnicoFactory.calculaDigitoUnicoSemCache(n, k));
		
		if (lengthListDU < 10) {
			digitoUnicoCache.add(resultDU);
			return resultDU.getResultado();
		}else {
			digitoUnicoCache.removeFirst();
			digitoUnicoCache.add(resultDU);
			return resultDU.getResultado();
		}
		
	}
	
	private static Integer recDigitoUnico(String P) {
		
		if (P.length() == 1) {
			return Integer.parseInt(P);
		}
		
		Integer newP = 0;
		
		for (int i = 0; i < P.length(); i++) {
						
			newP = newP + Integer.parseInt(P.charAt(i)+"");
		}
		
		return recDigitoUnico(newP.toString());
	}
	
	private static boolean validaInput(String nInput, Integer k) {

		int n = 0;
		
		try {
			n = Integer.parseInt(nInput);
		} catch (NumberFormatException e) {
			throw new NumberFormatException("Variável de entrada \'n\' não é um número inteiro !!");
		}
		
		if (n < 1 || n > Math.pow(10, 1000000)) {		
			throw new IllegalArgumentException("O valor de n está fora do range !!");
		}
		
		if (k < 1 || k > Math.pow(10, 5)) {
			throw new IllegalArgumentException("O valor de k está fora do range !!");
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		
		Integer r = calculaDigitoUnico("9875",4);
		System.out.println(r);
	}

}
