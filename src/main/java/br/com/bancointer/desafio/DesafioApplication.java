package br.com.bancointer.desafio;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.bancointer.desafio.service.DigitoUnicoService;
import br.com.bancointer.desafio.service.SettingService;
import br.com.bancointer.desafio.service.UserService;
import br.com.bancointer.desafio.util.InitDatabase;

@SpringBootApplication
public class DesafioApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioApplication.class, args);
	}

	@Bean
	public CommandLineRunner run(UserService userService, DigitoUnicoService digitoUnicoService, SettingService settingService)
			throws Exception {
		return (String[] args) -> {

			InitDatabase.initSettings(settingService);
			InitDatabase.initUserAndListDigitoUnico(userService, digitoUnicoService);
			
		};
	}

}
